/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entities;

import java.util.Date;
import java.util.List;
import library.entities.helpers.BookHelper;
import library.entities.helpers.CalendarFileHelper;
import library.entities.helpers.IBookHelper;
import library.entities.helpers.ICalendarHelper;
import library.entities.helpers.ILoanHelper;
import library.entities.helpers.IPatronHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;
import library.returnbook.IReturnBookControl;
import library.returnbook.IReturnBookUI;
import library.returnbook.ReturnBookControl;
import library.returnbook.ReturnBookUI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fabrizio
 */
public class LibraryTest {
    static IBookHelper bookHelper;
    static IPatronHelper patronHelper;
    static ILoanHelper loanHelper;
    static ICalendarHelper calendarHelper;
    
    Loan loan;
    IBook book;
    IPatron patron;
    
    String lastName = "User";
    String firstName = "Test";
    String email = "user@test.org";
    long phoneNo = 1234;
    int patronId = 1;
    String bookTitle = "The Stand";
    String bookAuthor = "Stephen King";
    String bookCallNo = "c1";
    int bookId = 1;
    int loanId = 1;
    
    
    public LibraryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        bookHelper = new BookHelper();
        patronHelper = new PatronHelper();
        loanHelper = new LoanHelper();
        calendarHelper = new CalendarFileHelper();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void Bug1_CorrectCalcuationOfFines() {
        // Expected result and delta comparison for comparing double value types
        double expResult = 1.0d; 
        double delta = 0.000001d;
        int daysToIncrement = 3;
              
        // Create library object
        Library library = new Library(bookHelper, patronHelper, loanHelper);
        IBook book = library.addBook(bookAuthor, bookTitle, bookCallNo);
        IPatron patron = library.addPatron(lastName, firstName, email, phoneNo);

        // Confirm user has no fines payable
        assertEquals(0.0d, patron.getFinesPayable(), delta);
        
        // Create loan against test book and user
        ILoan loan = loanHelper.makeLoan(book, patron);
        library.commitLoan(loan);

        // Increment the library calendar by three days
        ICalendar calendar = calendarHelper.loadCalendar();
        calendar.incrementDate(daysToIncrement);
        library.checkCurrentLoansOverDue();      
        
        // Create return book control object
        ReturnBookControl returnBookControl =  new ReturnBookControl(library);
        
        // Set the book state to READY and scan it in to calculate fines
        IReturnBookUI returnbookUI = new ReturnBookUI(returnBookControl);
        returnbookUI.setState(IReturnBookUI.UIStateConstants.READY);
        returnBookControl.bookScanned(book.getId());
        
        // Retrieve fines payable to user and compare against expected result
        double finesPayable = patron.getFinesPayable();     
        assertEquals(expResult, finesPayable, delta);
    }
    
    @Test
    public void Bug2_CorrectFineAmountLevied() {
        // Expected result and delta comparison for comparing double value types
        double delta = 0.000001d;
        int maxRand = 25;
        int minRand = 2;
        int daysToIncrement = (int) ((Math.random() * (maxRand - minRand)) + minRand);
        
        // Minus 2 as fines start on day 3
        double expResult = daysToIncrement - 2;
        
        System.out.println("daysToIncrement: " + daysToIncrement + ", expResult: " + expResult);
        //int daysToIncrement = 4;
               
        // Create library object
        Library library = new Library(bookHelper, patronHelper, loanHelper);
        IBook book = library.addBook(bookAuthor, bookTitle, bookCallNo);
        IPatron patron = library.addPatron(lastName, firstName, email, phoneNo);

        // Confirm user has no fines payable
        assertEquals(0.0d, patron.getFinesPayable(), delta);
        
        // Create loan against test book and user
        ILoan loan = loanHelper.makeLoan(book, patron);
        library.commitLoan(loan);

        // Increment the library calendar by three days
        ICalendar calendar = calendarHelper.loadCalendar();
        calendar.incrementDate(daysToIncrement);
        library.checkCurrentLoansOverDue();      
        
        // Create return book control object
        ReturnBookControl returnBookControl =  new ReturnBookControl(library);
        
        // Set the book state to READY and scan it in to calculate fines
        IReturnBookUI returnbookUI = new ReturnBookUI(returnBookControl);
        returnbookUI.setState(IReturnBookUI.UIStateConstants.READY);
        returnBookControl.bookScanned(book.getId());
        
        // Retrieve fines payable to user and compare against expected result
        double finesPayable = patron.getFinesPayable();     
        assertEquals(expResult, finesPayable, delta);
    } 
}
